'use strict';

pokemonApp.controller('EditPokemonCtrl', function($scope, PokemonsService, $routeParams) {

    PokemonsService.getPokemon($routeParams['pokemonId'])
      .then(function(response) {
          $scope.pokemon = response.data;
      });

    $scope.updatePokemon = function(myPokemon) {

        $scope.updateSuccess = false;

        PokemonsService.editPokemon(myPokemon).then(function(response) {

            $scope.pokemon = response.data;

            $scope.pokemonId = response.data.objectId;
            $scope.creationSuccess = true;

        });

    }

});
