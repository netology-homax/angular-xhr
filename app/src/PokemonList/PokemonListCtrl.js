'use strict';

pokemonApp.controller('PokemonListCtrl', function($scope, PokemonsService, BerriesService, $q, $location) {

    // PokemonsService.getPokemons().then(function(response) {
    //     $scope.pokemons = response.data.results;
    // });
    //
    // BerriesService.getBerries().then(function(response) {
    //     $scope.berries = response.data.results;
    // });

    $scope.loadingPokemons = true;
    $scope.loadingBerries = true;

    $q.all([PokemonsService.getPokemons(), BerriesService.getBerries()])
      .then(function(responses) {
        $scope.pokemons = responses[0].data.results;
        $scope.berries = responses[1].data.results;
        $scope.loadingPokemons = false;
        $scope.loadingBerries = false;

      }).catch(function(error) {
        $scope.loadingPokemons = false;
        $scope.loadingBerries = false;
      });



});
